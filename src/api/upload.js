

import request from '@/utils/request'

export const uploadImg = (params) => {
  return request({
    headers: {
      'Content-Type': 'multipart/form-data'// ;boundary=----WebKitFormBoundaryQ6d2Qh69dv9wad2u
    },
    transformRequest: [function (data) { // 在请求之前对data传参进行格式转换
      const formData = new FormData()
      Object.keys(data).forEach(key => {
        formData.append(key, data[key])
      })
      return formData
    }],
    url: '/storage/create',
    method: 'post',
    data: params
  })
}
