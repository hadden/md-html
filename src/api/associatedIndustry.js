import request from '@/utils/request'

export function listAssociatedIndustry(query) {
  return request({
    url: '/associatedIndustry/list',
    method: 'get',
    params: query
  })
}

export function updateAssociatedIndustry(data) {
  return request({
    url: '/associatedIndustry/update',
    method: 'post',
    data
  })
}

export function createAssociatedIndustry(data) {
  return request({
    url: '/associatedIndustry/create',
    method: 'post',
    data
  })
}

export function deleteAssociatedIndustry(data) {
  return request({
    url: '/associatedIndustry/delete',
    method: 'post',
    data
  })
}
export function getAnswerQuery(data) {
  return request({
    url: '/findChapterQuestPage',
    method: 'post',
    data
  })
}
export function updateAnswerData(data) {
  return request({
    url: '/updateChapterQuestion',
    method: 'post',
    data
  })
}
export function addAnswerData(data) {
  return request({
    url: '/addChapterQuestion',
    method: 'post',
    data
  })
}
export function delAnswerData(data) {
  return request({
    url: '/delChapterQuestion',
    method: 'post',
    data
  })
}
export function typeAssociatedIndustry(query) {
  return request({
    url: '/associatedIndustry/type',
    method: 'get',
    params: query
  })
}
