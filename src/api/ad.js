import request from '@/utils/request'

export function listAd(data) {
  return request({
    url: '/ad/list',
    method: 'post',
    data
  })
}

export function createAd(data) {
  return request({
    url: '/ad/create',
    method: 'post',
    data
  })
}

export function updateAd(data) {
  return request({
    url: '/ad/update',
    method: 'post',
    data
  })
}

export function deleteAd(data) {
  return request({
    url: '/ad/delete',
    method: 'post',
    data
  })
}
export function materialadd(data) {
  return request({
    url: '/material/add',
    method: 'post',
    data
  })
}
export function materiallist(data) {
  return request({
    url: '/material/list',
    method: 'post',
    data
  })
}
export function materialdelete(data) {
  return request({
    url: '/material/delete',
    method: 'post',
    data
  })
}
