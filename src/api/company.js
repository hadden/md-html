import request from '@/utils/request'

export function listCompany(query) {
  return request({
    url: '/company/list',
    method: 'get',
    params: query
  })
}

export function queryCompany(query) {
  return request({
    url: '/company/query',
    method: 'get',
    params: query
  })
}

export function createCompany(data) {
  return request({
    url: '/company/create',
    method: 'post',
    data
  })
}

export function updateCompany(data) {
  return request({
    url: '/company/update',
    method: 'post',
    data
  })
}

export function deleteCompany(data) {
  return request({
    url: '/company/delete',
    method: 'post',
    data
  })
}

export function changePwd(data) {
  return request({
    url: '/company/changePwd',
    method: 'post',
    data
  })
}

export function register(data) {
  return request({
    url: '/companyRegister/register',
    method: 'post',
    data
  })
}

export function queryCompanyInfo(query) {
  return request({
    url: '/company/queryCompanyInfo',
    method: 'get',
    params: query
  })
}

export function queryCourseRecord(query) {
  return request({
    url: '/company/courseRecord',
    method: 'get',
    params: query
  })
}
export function companycreateStudent(data) {
  return request({
    url: '/company/createStudent',
    method: 'post',
    data
  })
}
