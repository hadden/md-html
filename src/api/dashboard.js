import request from '@/utils/request'

export function info(query) {
  return request({
    url: '/dashboard',
    method: 'get',
    params: query
  })
}

export function importUserList(query) {
  return request({
    url: '/dashboard/importUser',
    method: 'get',
    params: query
  })
}
