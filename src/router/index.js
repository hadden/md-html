import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/views/layout/Layout'

/** note: Submenu only appear when children.length>=1
 *  detail see  https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 **/

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    perms: ['GET /aaa','POST /bbb']     will control the page perms (you can set multiple perms)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
    noCache: true                if true ,the page will no be cached(default is false)
  }
 **/
export const constantRouterMap = [{
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [{
      path: '/redirect/:path*',
      component: () =>
        import('@/views/redirect/index')
    }]
  },
  {
    path: '/login',
    component: () =>
      import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/enty/login',
    component: () =>
      import('@/views/login/enty-index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () =>
      import('@/views/login/authredirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () =>
      import('@/views/errorPage/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () =>
      import('@/views/errorPage/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [{
      path: 'dashboard',
      component: () =>
        import('@/views/dashboard/index'),
      name: 'Dashboard',
      meta: {
        title: 'dashboard',
        icon: 'dashboard',
        noCache: true
      }
    }]
  }
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRouterMap
})

export const asyncRouterMap = [

  {
    // trainCenter
    path: '/industryManage',
    component: Layout,
    // hidden: true,
    redirect: 'noredirect',
    alwaysShow: true,
    name: 'industryManage',
    meta: {
      title: '行业管理',
      icon: 'iconfont icon-zhuantiguanli'
    },
    children: [{
        path: 'industryCategory',
        component: () =>
          import('@/views/industry/industryCategory'),
        name: 'industryCategory',
        meta: {
          perms: ['GET /admin/associatedIndustry/list'],
          title: '行业列表',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      },
      {
        hidden: true,
        path: 'industryCreateOrUpdate',
        component: () =>
          import('@/views/industry/createOrUpdate'),
        name: 'industryCreateOrUpdate',
        meta: {
          perms: ['POST /admin/associatedIndustry/create', 'POST /admin/associatedIndustry/update'],
          title: '新增/编辑行业',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }
    ]
  },
  {
    path: '/student',
    component: Layout,
    // hidden: true,
    redirect: 'noredirect',
    alwaysShow: true,
    name: 'student',
    meta: {
      title: '学员管理',
      icon: 'iconfont icon-zhuantiguanli'
    },
    children: [{
        path: 'studentindex',
        component: () =>
          import('@/views/student/index'),
        name: 'studentindex',
        meta: {
          perms: ['POST /admin/student/list'],
          title: '学员列表',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      },
      {
        path: 'learningRecord',
        component: () =>
          import('@/views/record/index'),
        name: 'learningRecord',
        meta: {
          perms: ['POST /admin/learningRecord/findRecordForPage'],
          title: '学时列表',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }
    ]
  },
  /*  {
      // trainCenter
      path: '/industryManage/industryCategory',
      component: Layout,
      // hidden: true,
      // redirect: 'noredirect',
      // alwaysShow: true,
      name: 'industryManage',
      meta: {
        perms: ['GET /admin/associatedIndustry/list'],
        title: '行业管理',
        icon: 'iconfont icon-zhuantiguanli',
        noCache: true
      },
      children: [{
        hidden: true,
        path: 'industryCategory',
        component: () =>
          import('@/views/industry/industryCategory'),
        name: 'industryCategory',
        meta: {
          perms: ['GET /admin/associatedIndustry/list'],
          title: '行业列表',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      },
        {
          hidden: true,
          path: 'industryCreateOrUpdate',
          component: () =>
            import('@/views/industry/createOrUpdate'),
          name: 'industryCreateOrUpdate',
          meta: {
            perms: ['POST /admin/associatedIndustry/create', 'POST /admin/associatedIndustry/update'],
            title: '新增/编辑行业',
            icon: 'iconfont icon-guanggaoguanli',
            noCache: true
          }
        }
      ]
    },*/
  {
    path: '/courseManage',
    component: Layout,
    // hidden: true,
    redirect: 'noredirect',
    alwaysShow: true,
    name: 'courseManage',
    meta: {
      title: '课程管理',
      icon: 'iconfont icon-zhuantiguanli'
    },
    children: [{
        path: 'associatedIndustry',
        component: () =>
          import('@/views/course/associatedIndustry'),
        name: 'associatedIndustry',
        meta: {
          perms: ['GET /admin/associatedIndustry/list'],
          title: '行业课程',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      },
      {
        hidden: true,
        path: 'queryCourse',
        component: () =>
          import('@/views/course/query'),
        name: 'queryCourse',
        meta: {
          perms: ['GET /admin/course/query'],
          title: '查看课程',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      },
      {
        hidden: true,
        path: 'answerQuery',
        component: () =>
          import('@/views/course/answerQuery'),
        name: 'v',
        meta: {
          title: '视频答题管理',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      },
      {
        hidden: true,
        path: 'updateCourse',
        component: () =>
          import('@/views/course/update'),
        name: 'updateCourse',
        meta: {
          perms: ['POST /admin/course/updateOrCreate'],
          title: '编辑课程',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }, {
        hidden: true,
        path: 'question',
        component: () =>
          import('@/views/course/question'),
        name: 'question',
        meta: {
          perms: ['GET /admin/question/list'],
          title: '题库管理',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }, {
        path: 'commentsCourse',
        component: () =>
          import('@/views/course/comments'),
        name: 'commentsCourse',
        meta: {
          perms: ['GET /admin/courseComments/list'],
          title: '所有评论',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }

    ]
  },
  {
    path: '/adManage',
    component: Layout,
    // hidden: true,
    redirect: 'noredirect',
    alwaysShow: true,
    name: 'adManage',
    meta: {
      title: '轮播管理',
      icon: 'iconfont icon-zhuantiguanli'
    },
    children: [{
        path: 'ad',
        component: () =>
          import('@/views/ad/ad'),
        name: 'ad',
        meta: {
          perms: ['POST /admin/ad/list'],
          title: '轮播列表',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }

    ]
  },
  /* {
     path: '/adManage/ad',
     // component: Layout,
     // hidden: true,
     // redirect: 'noredirect',
     // alwaysShow: true,
     component: () =>
       import('@/views/ad/ad'),
     name: 'ad',
     meta: {
       perms: ['POST /admin/ad/list'],
       title: '轮播列表',
       icon: 'iconfont icon-guanggaoguanli',
       noCache: true
     },
     children: [
     //   {
     //   path: 'ad',
     //   component: () =>
     //     import('@/views/ad/ad'),
     //   name: 'ad',
     //   meta: {
     //     perms: ['POST /admin/ad/list'],
     //     title: '轮播列表',
     //     icon: 'iconfont icon-guanggaoguanli',
     //     noCache: true
     //   }
     // }
     ]
   },*/
  {
    path: '/orderManage',
    component: Layout,
    // hidden: true,
    redirect: 'noredirect',
    alwaysShow: true,
    name: 'orderManage',
    meta: {
      title: '订单管理',
      icon: 'iconfont icon-zhuantiguanli'
    },
    children: [{
        path: 'order',
        component: () =>
          import('@/views/order/order'),
        name: 'order',
        meta: {
          perms: ['GET /admin/order/list'],
          title: '订单列表',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }

    ]
  },
  {
    path: '/companyManage',
    component: Layout,
    // hidden: true,
    redirect: 'noredirect',
    alwaysShow: true,
    name: 'companyManage',
    meta: {
      title: '企业管理',
      icon: 'iconfont icon-zhuantiguanli'
    },
    children: [{
        path: 'company',
        component: () =>
          import('@/views/company/company'),
        name: 'company',
        meta: {
          perms: ['GET /admin/company/list'],
          title: '企业列表',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      },
      {
        hidden: true,
        path: 'query',
        component: () =>
          import('@/views/company/query'),
        name: 'query',
        meta: {
          perms: ['GET /admin/company/query'],
          title: '企业详情',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }

    ]
  },
  {
    path: '/organizationManage',
    component: Layout,
    // hidden: true,
    redirect: 'noredirect',
    alwaysShow: true,
    name: 'organizationManage',
    meta: {
      title: '基本设置',
      icon: 'iconfont icon-zhuantiguanli'
    },
    children: [{
        path: 'organization',
        component: () =>
          import('@/views/organization/organization'),
        name: 'organization',
        meta: {
          perms: ['GET /admin/organization/read'],
          title: '基本设置',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }

    ]
  },

  {
    path: '/sys',
    component: Layout,
    redirect: 'noredirect',
    alwaysShow: true,
    name: 'sysManage',
    meta: {
      title: '系统管理',
      icon: 'iconfont icon-xitong'
    },
    children: [{
        path: 'admin',
        component: () =>
          import('@/views/sys/admin'),
        name: 'admin',
        meta: {
          perms: ['GET /admin/admin/list', 'POST /admin/admin/create', 'POST /admin/admin/update',
            'POST /admin/admin/delete'
          ],
          title: '管理员',
          icon: 'iconfont icon-guanliyuan',
          noCache: true
        }
      },
      {
        path: 'role',
        component: () =>
          import('@/views/sys/role'),
        name: 'role',
        meta: {
          perms: ['GET /admin/role/list', 'POST /admin/role/create', 'POST /admin/role/update',
            'POST /admin/role/delete', 'GET /admin/role/permissions', 'POST /admin/role/permissions'
          ],
          title: '角色管理',
          icon: 'iconfont icon-jiaose',
          noCache: true
        }
      }
      // ,
      // {
      //   path: 'os',
      //   component: () => import('@/views/sys/os'),
      //   name: 'os',
      //   meta: {
      //     perms: ['GET /admin/os/list', 'POST /admin/os/create', 'POST /admin/os/update', 'POST /admin/os/delete'],
      //     title: '对象存储',
      //     noCache: true
      //   }
      // }
    ]
  },

  // {
  //   path: '/companyHomeManage',
  //   component: Layout,
  //   // hidden: true,
  //   redirect: 'noredirect',
  //   alwaysShow: true,
  //   name: 'companyHomeManage',
  //   meta: {
  //     perms: ['GET /admin/companyHome/info'],
  //     title: '首页',
  //     icon: 'iconfont icon-zhuantiguanli',
  //     noCache: true
  //   }
  // },

  {
    path: '/companyOperateManage',
    component: Layout,
    // hidden: true,
    redirect: 'noredirect',
    alwaysShow: true,
    name: 'companyOperateManage',
    meta: {
      title: '企业运营中心',
      icon: 'iconfont icon-zhuantiguanli'
    },
    children: [{
        hidden: true,
        path: 'companyRegister',
        component: () =>
          import('@/views/companyOperate/companyRegister'),
        name: 'companyRegister',
        meta: {
          perms: ['POST /admin/companyRegister/register'],
          title: '注册',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }, {
        hidden: true,
        path: 'companyHome',
        component: () =>
          import('@/views/companyOperate/companyHome'),
        name: 'companyHome',
        meta: {
          perms: ['GET /admin/companyHome/info'],
          title: '首页',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }, {
        hidden: true,
        path: 'completeInfo',
        component: () =>
          import('@/views/companyOperate/completeInfo'),
        name: 'completeInfo',
        meta: {
          perms: ['POST /admin/companyInfo/completeInfo'],
          title: '完善信息',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }, {
        path: 'companyStudentList',
        component: () =>
          import('@/views/companyOperate/companyStudentList'),
        name: 'companyStudentList',
        meta: {
          perms: ['POST /admin/companyStudent/list'],
          title: '学员列表',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      },
      //   {
      //   hidden: true,
      //   path: 'companyStudentOrders',
      //   component: () =>
      //                 import('@/views/companyOperate/companyStudentOrders'),
      //   name: 'companyStudentOrders',
      //   meta: {
      //     perms: ['GET /admin/companyStudent/orders'],
      //     title: '学员缴费',
      //     icon: 'iconfont icon-guanggaoguanli',
      //     noCache: true
      //   }
      // },
      {
        path: 'companyOrder',
        component: () =>
          import('@/views/companyOperate/companyOrder'),
        name: 'companyOrder',
        meta: {
          perms: ['GET /admin/companyOrder/list'],
          title: '订单列表',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      },
      {
        // hidden: true,
        path: 'companyInfo',
        component: () =>
          import('@/views/companyOperate/companyInfo'),
        name: 'companyInfo',
        meta: {
          perms: ['GET /admin/companyInfo/query'],
          title: '企业信息',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      },
      {
        hidden: true,
        path: 'companyBatchOrder',
        component: () =>
          import('@/views/companyOperate/companyBatchOrder'),
        name: 'companyBatchOrder',
        meta: {
          perms: ['GET /admin/companyOrder/ordersStudent'],
          title: '批量缴费',
          icon: 'iconfont icon-guanggaoguanli',
          noCache: true
        }
      }
    ]
  },
  {
    path: '/setcompanyuser',
    component: Layout,
    // hidden: true,
    redirect: 'noredirect',
    alwaysShow: true,
    name: 'setcompanyuser',
    meta: {
      title: '账号管理',
      icon: 'iconfont icon-zhuantiguanli'
    },
    children: [{
      path: 'list',
      component: () =>
        import('@/views/setcompanyuser/list'),
      name: 'list',
      meta: {
        perms: ['GET /admin/pcompany/list'],
        title: '子账号管理',
        icon: 'iconfont icon-guanggaoguanli',
        noCache: true
      }
    }, ]
  },
  {
    path: 'external-link',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    alwaysShow: true,
    name: 'externalLink',
    meta: {
      title: '外链',
      icon: 'link'
    },
    children: [{
        path: 'https://cloud.tencent.com/product/cos',
        meta: {
          title: '腾讯云存储',
          icon: 'link'
        }
      },
      {
        path: 'https://cloud.tencent.com/product/sms',
        meta: {
          title: '腾讯云短信',
          icon: 'link'
        }
      },
      {
        path: 'https://pay.weixin.qq.com/index.php/core/home/login',
        meta: {
          title: '微信支付',
          icon: 'link'
        }
      },
      {
        path: 'https://mpkf.weixin.qq.com/',
        meta: {
          title: '小程序客服',
          icon: 'link'
        }
      },
      {
        path: 'https://www.alibabacloud.com/zh/product/oss',
        meta: {
          title: '阿里云存储',
          icon: 'link'
        }
      },
      {
        path: 'https://www.qiniu.com/products/kodo',
        meta: {
          title: '七牛云存储',
          icon: 'link'
        }
      },
      {
        path: 'http://www.kdniao.com/api-track',
        meta: {
          title: '快递鸟',
          icon: 'link'
        }
      }
    ]
  },

  {
    path: '/profile',
    component: Layout,
    redirect: 'noredirect',
    alwaysShow: true,
    children: [{
      path: 'password',
      component: () =>
        import('@/views/profile/password'),
      name: 'password',
      meta: {
        title: '修改密码',
        noCache: true
      }
    }],
    hidden: true
  },

  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]
