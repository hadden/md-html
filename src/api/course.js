import request from '@/utils/request'

export function queryCourse(query) {
  return request({
    url: '/course/query',
    method: 'get',
    params: query
  })
}
export function listCourse(query) {
  return request({
    url: '/course/list',
    method: 'get',
    params: query
  })
}

export function createCourse(data) {
  return request({
    url: '/course/create',
    method: 'post',
    data
  })
}

export function updateCourse(data) {
  return request({
    url: '/course/update',
    method: 'post',
    data
  })
}

export function deleteCourse(data) {
  return request({
    url: '/course/delete',
    method: 'post',
    data
  })
}
export function listCourseComments(query) {
  return request({
    url: '/courseComments/list',
    method: 'get',
    params: query
  })
}

export function updateCourseComments(data) {
  return request({
    url: '/courseComments/update',
    method: 'post',
    data
  })
}

export function batchCourseComments(data) {
  return request({
    url: '/courseComments/batch',
    method: 'post',
    data
  })
}

export function updateOrCreateCourse(data) {
  return request({
    url: '/course/updateOrCreate',
    method: 'post',
    data
  })
}

export function createCourseSection(data) {
  return request({
    url: '/course/createCourseSection',
    method: 'post',
    data
  })
}

export function updateCourseSection(data) {
  return request({
    url: '/course/updateCourseSection',
    method: 'post',
    data
  })
}
export function deleteCourseSection(data) {
  return request({
    url: '/course/deleteCourseSection',
    method: 'post',
    data
  })
}
export function updateSection(data) {
  return request({
    url: '/course/updateSection',
    method: 'post',
    data
  })
}
