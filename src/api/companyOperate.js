import request from '@/utils/request'

export function loginByUsername(data) {
    return request({
        url: '/auth/login',
        method: 'post',
        data
    })
}
export function listCompanyOrder(query) {
    return request({
        url: '/companyOrder/list',
        method: 'get',
        params: query
    })
}

export function listCompanyOrderStudent(query) {
    return request({
        url: '/companyOrder/ordersStudent',
        method: 'get',
        params: query
    })
}

export function queryPayStatustCompanyStudent(query) {
    return request({
        url: '/companyStudent/queryPayStatus',
        method: 'get',
        params: query
    })
}
export function importStudents(data) {
    return request({
        url: '/companyStudent/importStudent',
        method: 'post',
        data
    })
}

export function listCompanyStudent(data) {
    return request({
        url: '/companyStudent/list',
        method: 'post',
        data
    })
}

export function createCompanyStudent(data) {
    return request({
        url: '/companyStudent/create',
        method: 'post',
        data
    })
}

export function updateCompanyStudent(data) {
    return request({
        url: '/companyStudent/update',
        method: 'post',
        data
    })
}

export function queryCert(query) {
    return request({
        url: '/companyStudent/queryCert',
        method: 'get',
        params: query
    })
}

export function orderCompanyStudent(data) {
    return request({
        url: '/companyStudent/order',
        method: 'post',
        data
    })
}

export function companyRegister(data) {
    return request({
        url: '/companyRegister/register',
        method: 'post',
        data
    })
}
export function companyCompleteInfo(data) {
    return request({
        url: '/companyInfo/completeInfo',
        method: 'post',
        data
    })
}

export function queryCompanyInfo(query) {
    return request({
        url: '/companyInfo/query',
        method: 'get',
        params: query
    })
}

export function queryCourseCompanyInfo(query) {
    return request({
        url: '/companyInfo/queryCourse',
        method: 'get',
        params: query
    })
}

export function industryCategory(query) {
    return request({
        url: '/companyInfo/industryCategory',
        method: 'get',
        params: query
    })
}

export function industryCategoryAll(query) {
    return request({
        url: '/companyInfo/industryCategoryAll',
        method: 'get',
        params: query
    })
}

export function queryCourseAll(query) {
    return request({
        url: '/companyInfo/queryCourseAll',
        method: 'get',
        params: query
    })
}
export function companyHome(query) {
    return request({
        url: '/companyHome/info',
        method: 'get',
        params: query
    })
}

export function readOrganization(data) {
    return request({
        url: '/companyOrder/readOrganization',
        method: 'post',
        data
    })
}

export function offlinePay(data) {
    return request({
        url: '/companyOrder/offlinePay',
        method: 'post',
        data
    })
}

export function wxPay(data) {
    return request({
        url: '/companyOrder/wxPay',
        method: 'post',
        data
    })
}

export function nativePay(data) {
    return request({
        url: '/native/pay',
        method: 'post',
        data
    })
}

export function queryCityData(query) {
    return request({
        url: '/companyInfo/queryCity',
        method: 'get',
        params: query
    })
}

export function wxRes(query) {
    return request({
        url: 'order/pay/status',
        method: 'get',
        params: query
    })
}