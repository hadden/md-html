import request from '@/utils/request'

export function listIndustry(query) {
  return request({
    url: '/industryCategory/list',
    method: 'get',
    params: query
  })
}

export function queryIndustry(query) {
  return request({
    url: '/industryCategory/query',
    method: 'get',
    params: query
  })
}
export function updateOrCreateIndustry(data) {
  return request({
    url: '/industryCategory/updateOrCreate',
    method: 'post',
    data
  })
}

export function deleteIndustry(data) {
  return request({
    url: '/industryCategory/delete',
    method: 'post',
    data
  })
}

export function createCategory(data) {
  return request({
    url: '/industryCategory/createCategory',
    method: 'post',
    data
  })
}
export function updateCategory(data) {
  return request({
    url: '/industryCategory/updateCategory',
    method: 'post',
    data
  })
}
export function createIndustry(data) {
  return request({
    url: '/industryCategory/create',
    method: 'post',
    data
  })
}

export function updateIndustry(data) {
  return request({
    url: '/industryCategory/update',
    method: 'post',
    data
  })
}

