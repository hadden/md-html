import request from '@/utils/request'

export function readOrganization(query) {
  return request({
    url: '/organization/read',
    method: 'get',
    params: query
  })
}

export function updateOrganization(data) {
  return request({
    url: '/organization/update',
    method: 'post',
    data
  })
}

