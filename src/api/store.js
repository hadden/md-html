import request from '@/utils/request'

export function listStore(query) {
  return request({
    url: 'store/list',
    method: 'get',
    params: query
  })
}

export function createStore(data) {
  return request({
    url: '/store/create',
    method: 'post',
    data
  })
}

// export function readAd(data) {
//   return request({
//     url: '/store/read',
//     method: 'get',
//     data
//   })
// }

export function updateStore(data) {
  return request({
    url: '/store/update',
    method: 'post',
    data
  })
}

export function deleteStore(data) {
  return request({
    url: '/store/delete',
    method: 'get',
    params: data
  })
}
