import request from '@/utils/request'

export function loginByUsername(username, password,loginType) {
  const data = {
    username,
    password,
    loginType
  }
  return request({
    url: '/auth/login',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/auth/logout',
    method: 'post'
  })
}

export function getUserInfo(token) {
  return request({
    url: '/auth/info',
    method: 'get',
    params: { token }
  })
}
export function getYzmFunc(mobile,captchaType) {
  const data = {
    mobile,
    captchaType
  }
  return request({
    url: '/companyRegister/captcha',
    method: 'post',
    data
  })
}
export function postPassword(password,mobile,code) {
  const data = {
    password,
    mobile,
    code
  }
  return request({
    url: '/companyRegister/restPassword',
    method: 'post',
    data
  })
}