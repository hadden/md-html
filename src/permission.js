import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // getToken from cookie

NProgress.configure({ showSpinner: false }) // NProgress Configuration

// permission judge function
function hasPermission(perms, permissions) {
    if (perms.indexOf('*') >= 0) return true // admin permission passed directly
    if (!permissions) return true
    return perms.some(perm => permissions.indexOf(perm) >= 0)
}

const whiteList = ['/enty/login', '/login', '/auth-redirect'] // no redirect whitelist

router.beforeEach((to, from, next) => {
    NProgress.start() // start progress bar
	console.log(getToken())
    if (getToken()) { // determine if there has token
        /* has token*/
        if (to.path === '/login' || to.path === '/enty/login') {
            next({ path: '/' })
            NProgress.done() // if current page is dashboard will not trigger	afterEach hook, so manually handle it
        } else {
            if (store.getters.perms.length === 0) { // 判断当前用户是否已拉取完user_info信息
                store.dispatch('GetUserInfo').then(res => { // 拉取user_info
                    const perms = res.data.data.perms // note: perms must be a array! such as: ['GET /aaa','POST /bbb']
                    store.dispatch('GenerateRoutes', { perms }).then(() => { // 根据perms权限生成可访问的路由表
                        router.addRoutes(store.getters.addRouters) // 动态添加可访问路由表
                        next({...to, replace: true }) // hack方法 确保addRoutes已完成 ,set the replace: true so the navigation will not leave a history record
                    })
                }).catch((err) => {
                    store.dispatch('FedLogOut').then(() => {
                        Message.error(err || 'Verification failed, please login again')
                        next({ path: '/' })
                    })
                })
            } else {
                // 没有动态改变权限的需求可直接next() 删除下方权限判断 ↓
                console.log(store.getters.perms)
                console.log(to)
                if (hasPermission(store.getters.perms, to.meta.perms)) {
                    next()
                } else {
                    next({ path: '/401', replace: true, query: { noGoBack: true } })
                }
                // 可删 ↑
            }
        }
    } else {
        console.log(to.path)
            /* has no token*/
        const hostName = 'md.czttao.com'
        // const hostName = 'localhost'
        if (whiteList.indexOf(to.path) !== -1) { // 在免登录白名单，直接进入
            console.log(window.location.hostname)
            if (window.location.hostname === hostName) {
                if (to.path === '/enty/login') {
                    next()
                } else {
                    next({ path: '/enty/login' }) // 否则全部重定向到企业登录页
                }
            } else {
                if (to.path === '/login') {
                    next()
                } else {
                    next(`/login`) // 否则全部重定向到培训登录页
                }
            }
        } else {
            console.log(window.location.hostname)
            if (window.location.hostname === hostName) {
                next(`/enty/login?redirect=${to.path}`) // 否则全部重定向到企业登录页
            } else {
                next(`/login?redirect=${to.path}`) // 否则全部重定向到培训登录页
            }

            NProgress.done() // if current page is login will not trigger afterEach hook, so manually handle it
        }
    }
})

router.afterEach(() => {
    NProgress.done() // finish progress bar
})
