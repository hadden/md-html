import request from '@/utils/request'

export function queryInfo(query) {
  return request({
    url: '/home/info',
    method: 'get',
    params: query
  })
}
