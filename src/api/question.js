import request from '@/utils/request'

export function listQuestion(query) {
  return request({
    url: '/question/list',
    method: 'get',
    params: query
  })
}

export function createQuestion(data) {
  return request({
    url: '/question/create',
    method: 'post',
    data
  })
}

export function updateQuestion(data) {
  return request({
    url: '/question/update',
    method: 'post',
    data
  })
}

export function deleteQuestion(data) {
  return request({
    url: '/question/delete',
    method: 'post',
    data
  })
}
export function createList(data) {
  return request({
    url: '/question/createList',
    method: 'post',
    data
  })
}
