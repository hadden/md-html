import request from '@/utils/request'

export function studentlist(data) {
  return request({
    url: 'student/list',
    method: 'post',
    data
  })
}
export function studentupdate(data) {
  return request({
    url: 'student/update',
    method: 'post',
    data
  })
}
export function batchUpdate(data) {
  return request({
    url: 'student/batchUpdate',
    method: 'post',
    data
  })
}
