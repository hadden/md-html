import request from '@/utils/request'

export function listCompany(query) {
  return request({
    url: '/pcompany/list',
    method: 'get',
    params: query
  })
}

export function queryCompany(query) {
  return request({
    url: '/pcompany/query',
    method: 'get',
    params: query
  })
}

export function createCompany(data) {
  return request({
    url: '/pcompany/create',
    method: 'post',
    data
  })
}

export function updateCompany(data) {
  return request({
    url: '/pcompany/update',
    method: 'post',
    data
  })
}

export function deleteCompany(data) {
  return request({
    url: '/pcompany/delete',
    method: 'post',
    data
  })
}

export function changePwd(data) {
  return request({
    url: '/pcompany/changePwd',
    method: 'post',
    data
  })
}

export function register(data) {
  return request({
    url: '/pcompany/create',
    method: 'post',
    data
  })
}

export function queryCompanyInfo(query) {
  return request({
    url: '/pcompany/queryCompanyInfo',
    method: 'get',
    params: query
  })
}

export function queryCourseRecord(query) {
  return request({
    url: '/pcompany/courseRecord',
    method: 'get',
    params: query
  })
}
export function queryDetail(query) {
  return request({
    url: '/pcompany/query',
    method: 'get',
    params: query
  })
}
