import request from '@/utils/request'

export function learningRecord(data) {
  return request({
    url: '/learningRecord/findRecordForPage',
    method: 'post',
    data
  })
}
